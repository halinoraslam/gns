var src         = 'app';
var build       = 'build';
var devAssets   = src   + '/assets';
var devLayouts  = src   + '/layouts';
var buildAssets = build + '/assets';

module.exports = {
  browserSync: {
    server: {
      baseDir: build, // also can accept massive for multiply dir. example: ["app", "dist"]
      directory: true
    },
    port: 9999
  },
  delete: {
    src: [buildAssets]
  },
  styles: { // global styles config
    sass: { // sass config
      src:  devAssets + '/styles/sass/**/*.{scss, sass}',
      dest: build     + '/assets/css',
      options: {
        indentedSyntax: false,  // Enable .sass syntax!
        outputStyle: 'expanded' // 'or compressed'
      }
    },
    vendor: { // autoprefixer
      browsers: [
        'last 30 version',
        'ie 8',
        'ie 9',
        'ios 6',
        'android 4'
      ],
      cascade: false
    }
  },
  scripts: {
    src:  devAssets + '/js/**/*.js',
    dest: build     + '/assets/js'
  },
  html: {   // global html config
    jade: { // jade config
      src:  devLayouts + '/pages/**/*.jade',
      dest: build,
      options: {
        pretty: true // beautify output html
      }
    }
  },
  images: { // images simple settings
    src:  devAssets + '/images/**/*',
    dest: build + '/assets/images',
    options: {
      progressive: true,
      optimizationLevel: 7
    }
  },
  watch: {
    sass:    devAssets  + '/styles/sass/**/*.{scss, sass}',
    jade:    devLayouts + '/**/*.jade',
    scripts: devAssets  + '/js/**/*.js',
    images:  devAssets  + '/images/**/*'
  }
};