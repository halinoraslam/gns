var gulp   = require('gulp');
var del    = require('del');
var runSequence = require('run-sequence');
var config = require('../../config').delete;

gulp.task('delete', function(cb){
  del(config.src, cb);
  runSequence('build', cb);
});