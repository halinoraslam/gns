var gulp         = require('gulp');
var browserSync  = require('browser-sync');
var imagemin     = require('gulp-imagemin');
var config       = require('../../config').images;

gulp.task('images', function() {
  return gulp.src(config.src)
        .pipe(imagemin(config.options))
        .pipe(gulp.dest(config.dest))
        .pipe(browserSync.reload({stream:true}));
});