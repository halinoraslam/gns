var gulp        = require('gulp');
var jade        = require('gulp-jade');
var notify      = require('gulp-notify');
var changed     = require('gulp-changed');
var config      = require('../../config').html;
var browserSync = require('browser-sync');

gulp.task('jade', function() {
  return gulp.src(config.jade.src)
        .pipe(changed(config.jade.dest, {extension: '.html'}))
        .pipe(jade(config.jade.options).on('error', notify.onError(function (error) {
          return 'An error occurred while compiling jade.\nLook in the console for details.\n' + error;
        })))
        .pipe(gulp.dest(config.jade.dest))
        .pipe(browserSync.reload({stream:true}));
});