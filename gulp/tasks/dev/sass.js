var gulp         = require('gulp');
var sass         = require('gulp-sass');
var browserSync  = require('browser-sync');
var sourcemaps   = require('gulp-sourcemaps');
var config       = require('../../config').styles;
var autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function() {
  return gulp.src(config.sass.src)
        .pipe(sourcemaps.init())
        .pipe(sass(config.sass.options).on('error', sass.logError))
        .pipe(autoprefixer(config.vendor))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.sass.dest))
        .pipe(browserSync.reload({stream:true}));
});